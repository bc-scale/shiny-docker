This repository contains the sources of a Docker image containing Shiny Server.

The image is intended to be extended by other Docker images to host Shiny applications.

# Configuration

The server uses the default configuration.
It is located at `/etc/shiny-server/shiny-server.conf`

If that configuration does not fit your purpose, you need to override it in derived Docker images.

Currently, the default configuration makes Shiny Server

- listen on port 3838,
- has its web root directory set to `/RSV/shiny-server/`,
- and writes its logs into `/var/log/shiny-server/`.

# Use

To use the Shiny Server image make sure that Docker has access to the image wherever you plan to build a derived image.

## Build Shiny Server Image

You can, for example, build the image your self with the tag `shiny-server:latest` by running

```
docker build -t shiny-server:latest .
```

in the root directory of a clone of this repository.

## Test Shiny Server Image

You can test whether the build was successful by running the image:

```
docker run --rm -p 3838:3838 shiny-server:latest
```

You should now find under `http://localhost:3838` the example page of Shiny Server.

## Define Derived Image

To run your own application create a Dockerfile that installs all dependencies and puts the application in Shiny Server’s web root directory, by default `/srv/shiny-server/`, for example (assuming this repository’s Docker image is available under the tag `shiny-server:latest`):

```
FROM shiny-server:latest

RUN cd /srv/shiny-server \
    && rm index.html sample-apps \
    && echo '<!doctype html><html><head><title>Example</title></head><body><h1>Example Page</h1></body></html>' > index.html

USER shiny
```

In the example, we delete the default content of `/srv/shiny-server/` (both symbolic links, thus no recursive deletion).
Then we add our own content; in this case in the form of a short HTML file.
Finally, we set the user to `shiny`, so that Shiny Server is not run by root.

For actual Shiny applications, you might need to install dependencies first.
You can install general dependencies through Ubuntu’s package management, since Ubuntu builds the basis for the Shiny Server image.
Be sure to run `apt-get update` first, because apt’s database is deleted during build of the Shiny Server image.
You can install R package dependencies by using R’s `install.package` mechanism.
Dependencies should be installed before switching to user `shiny`.

## Run Derived Image

After building your derived image, for example under the tag `shiny-test-application:latest`, you can run it via

```
docker run --rm -p 3838:3838 shiny-test-application:latest
```

and visit your application under `http://localhost:3838`.
If you put your application in a subdirectory `/srv/shiny-server`, you need to add its relative path within that directory to the end of `http://localhost:3838`.


# Copyright and License

Copyright © 2022 Friedrich-Schiller-Universität Jena

The sources of this repository are available under the [MIT License](LICENSE).
